<div class="at-panel panel-display five-5x20 clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-row row-1 clearfix">
    <div class="region region-five-first">
      <div class="region-inner clearfix">
        <?php print $content['five_first']; ?>
      </div>
    </div>
    <div class="region region-five-second">
      <div class="region-inner clearfix">
        <?php print $content['five_second']; ?>
      </div>
    </div>
  </div>
  <div class="panel-row row-2 clearfix">
    <div class="region region-five-third">
      <div class="region-inner clearfix">
        <?php print $content['five_third']; ?>
      </div>
    </div>
    <div class="region region-five-fourth">
      <div class="region-inner clearfix">
        <?php print $content['five_fourth']; ?>
      </div>
    </div>
    <div class="region region-five-fifth">
      <div class="region-inner clearfix">
        <?php print $content['five_fifth']; ?>
      </div>
    </div>
  </div>
</div>
