<div class="at-panel panel-display six-6x16 multicolumn clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-row row-1 clearfix">
    <div class="region region-six-first">
      <div class="region-inner clearfix">
        <?php print $content['six_first']; ?>
      </div>
    </div>
    <div class="region region-six-second">
      <div class="region-inner clearfix">
        <?php print $content['six_second']; ?>
      </div>
    </div>
  </div>
  <div class="panel-row row-2 clearfix">
    <div class="region region-six-third">
      <div class="region-inner clearfix">
        <?php print $content['six_third']; ?>
      </div>
    </div>
    <div class="region region-six-fourth">
      <div class="region-inner clearfix">
        <?php print $content['six_fourth']; ?>
      </div>
    </div>
  </div>
  <div class="panel-row row-3 clearfix">
    <div class="region region-six-fifth">
      <div class="region-inner clearfix">
        <?php print $content['six_fifth']; ?>
      </div>
    </div>
    <div class="region region-six-sixth">
      <div class="region-inner clearfix">
        <?php print $content['six_sixth']; ?>
      </div>
    </div>
  </div>
</div>
