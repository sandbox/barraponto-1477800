<div class="at-panel panel-display one-column clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="region region-one-main">
    <div class="region-inner clearfix">
      <?php print $content['one_main']; ?>
    </div>
  </div>
</div>
