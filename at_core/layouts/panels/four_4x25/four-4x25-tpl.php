<div class="at-panel panel-display four-4x25 clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <div class="panel-row row-1 clearfix">
    <div class="region region-four-first">
      <div class="region-inner clearfix">
        <?php print $content['four_first']; ?>
      </div>
    </div>
    <div class="region region-four-second">
      <div class="region-inner clearfix">
        <?php print $content['four_second']; ?>
      </div>
    </div>
  </div> 
  <div class="panel-row row-2 clearfix">
    <div class="region region-four-third">
      <div class="region-inner clearfix">
        <?php print $content['four_third']; ?>
      </div>
    </div>
    <div class="region region-four-fourth">
      <div class="region-inner clearfix">
        <?php print $content['four_fourth']; ?>
      </div>
    </div>
  </div>
</div>
