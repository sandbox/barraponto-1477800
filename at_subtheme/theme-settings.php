<?php
/**
 * USAGE:
 * To use this file replace "adaptivetheme_subtheme" with the name of
 * your theme in the function below.
 */
function adaptivetheme_subtheme_form_system_theme_settings_alter(&$form, &$form_state)  {
  // Your knarly custom theme settings go here...
}